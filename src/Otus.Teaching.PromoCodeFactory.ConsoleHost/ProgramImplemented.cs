﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Otus.Teaching.PromoCodeFactory.ConsoleHost.Controllers;
using Otus.Teaching.PromoCodeFactory.ConsoleHost.Views;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.ConsoleHost
{
    class ProgramImplemented
    {
        static async Task Main2(string[] args)
        {
            var serviceCollection = new ServiceCollection();

            ConfigureServices(serviceCollection);

            await using var serviceProvider = serviceCollection.BuildServiceProvider();
            using var scope = serviceProvider.CreateScope();
            var employeeController = scope.ServiceProvider.GetService<EmployeeConsoleController>();
            await employeeController.GetEmployeesListAsync();
            var employeeController2 = scope.ServiceProvider.GetService<EmployeeConsoleController>();
            await employeeController2.GetEmployeesListAsync();
            
            using var scope2 = serviceProvider.CreateScope();
            var employeeController3 = scope2.ServiceProvider.GetService<EmployeeConsoleController>();
            await employeeController3.GetEmployeesListAsync();
        }
        
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<EmployeeConsoleController>();
            services.AddScoped(typeof(IRepository<Employee>), (x) => 
                new InMemoryRepository<Employee>(FakeDataFactory.Employees));
            services.TryAddScoped(typeof(IRepository<Employee>), (x) => 
                new NotImplementedRepository<Employee>());
        }
    }
}